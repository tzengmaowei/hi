# GitLab

ref: https://cloudinfrastructureservices.co.uk/how-to-install-gitlab-on-ubuntu-22-04/

# GitLab Runner

ref: https://www.fosstechnix.com/install-gitlab-runner-on-ubuntu-22-04-lts/
     https://cloudinfrastructureservices.co.uk/how-to-create-gitlab-runner-container-on-docker-tutorial/
     
# Jenkins

ref: https://cloudinfrastructureservices.co.uk/how-to-install-jenkins-on-ubuntu-22-04-tutorial/